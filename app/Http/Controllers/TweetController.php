<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tweet;
use App\User;

class TweetController extends Controller
{
    public function create()
    {
        if (!\Auth::check()) {
            return redirect('/login');
        }

        return view('tweetForm');
    }

    public function store()
    {
        if (!\Auth::check()) {
            return redirect('/login');
        }

        $request = request();
        $loggedInUser = $request->user();
        $data = $request->all();

        $tweet = new Tweet();
        $tweet->content = $data['content'];
        $tweet->user_id = $loggedInUser->id;
        $tweet->save();

        return redirect('/');
    }

    public function userTweets($userId)
    {
        $user = User::where('handle', $userId)
            ->orWhere('id', $userId)
            ->first();

        if (!$user) {
            abort(404);
        }

        $tweets = $user->tweets;

        return view('userTweets', [
            'user' => $user,
            'tweets' => $tweets
        ]);
    }

    public function toggleLike($tweetId)
    {
        $user = request()->user();
        $tweet = Tweet::find($tweetId);

        if ($tweet->isLikedByCurrentUser()) {
            $tweet->likes()->detach($user);
        } else {
            $tweet->likes()->attach($user);
        }

        return back()
            ->with('message', 'You successfully liked a tweet.');
    }
}
