<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tweet;
use App\User;

class MainController extends Controller
{
    public function index() {
        $users = User::all();
        $tweets = Tweet::all();

        $metrics = [
            ['metric' => 'Tweets', 'value' => '37K'],
            ['metric' => 'Following', 'value' => '37K'],
            ['metric' => 'Followers', 'value' => '37K'],
            ['metric' => 'Likes', 'value' => '37K'],
            ['metric' => 'Moments', 'value' => '37K'],
        ];

        $data = [
            'users' => $users,
            'tweets' => $tweets,
            'metrics' => $metrics
        ];

        return view('welcome', $data);
    }
}
