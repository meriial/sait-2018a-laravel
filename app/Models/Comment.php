<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tweet;
use App\User;

class Comment extends Model
{
    public function tweet()
    {
        return $this->belongsTo(Tweet::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
