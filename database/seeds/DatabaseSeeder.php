<?php

use Illuminate\Database\Seeder;
use App\Models\Tweet;
use App\Models\Comment;
use App\User;
use Faker\Factory;

class DatabaseSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $faker = Factory::create();

        $user = new User();
        $user->name = $faker->name;
        $user->handle = $faker->word;
        $user->image = $faker->imageUrl(100,100,'cats');
        $user->email = $faker->unique()->email;
        $user->password = 'haha';
        $user->save();

        for ($i=0; $i < rand(1,10); $i++) {
            $tweet = new Tweet();
            $tweet->content = $faker->catchPhrase;
            $tweet->user_id = $user->id;
            $tweet->save();

            // $tweet->likes()->attach($user);
            $user->likes()->attach($tweet);

            for ($j=0; $j<rand(1,10) ; $j++) {
                $comment = new Comment();
                $comment->content = $faker->catchPhrase;
                $comment->user_id = $user->id;
                $comment->tweet_id = $tweet->id;
                $comment->save();
            }
        }
    }
}
