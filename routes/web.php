<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/about', 'AboutController@index');

Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

// Route::get('/comment', 'CommentController@create');
// Route::post('/comment', 'CommentController@store');
//
Route::get('/tweet', 'TweetController@create');
Route::post('/tweet', 'TweetController@store');
Route::get('/tweets/{id}', 'TweetController@userTweets');
Route::get('/tweets/{id}/like/toggle', 'TweetController@toggleLike');


Route::get('/', 'MainController@index');

Route::get('/sandbox', function() {
    // dd($_COOKIE);
    // setcookie('bob', 'hahahahah', time()-3600);

    // session_start();
    // $_SESSION['foo'] = 'bar';
    // unset($_SESSION);
    // session_destroy();
    // dd($_SESSION);

    // session([
    //     'foo' => 'bar',
    //     'bat' => 'baz'
    // ]);

    $value = session('bat');

    dd($value);
});








//

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');






//
