<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SAIT Winter 2018</title>
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        @if (Auth::check())
            Hello, {{ Auth::user()->name }}
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @else
            <a href="/login">Login</a>
        @endif
        @yield('content')
    </body>
</html>
