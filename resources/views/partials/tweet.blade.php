{{ $tweet->user->handle }}
{{ $tweet->content }}

@if ($tweet->isLikedByCurrentUser())
    <a href="/tweets/{{ $tweet->id }}/like/toggle">unlike</a>
@else
    <a href="/tweets/{{ $tweet->id }}/like/toggle">like</a>
@endif

<ul>
    @foreach ($tweet->comments as $comment)
        <li>
            Comment: {{ $comment->content }} <br>
            User: {{ $comment->user->name }} <br>
            Date: {{ $comment->updated_at->diffForHumans() }}
        </li>
    @endforeach
</ul>
