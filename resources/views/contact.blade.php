@extends('layout')

@section('content')

    <?php if($message = session('message')): ?>
        <div class="alert alert-success">
            <?php echo $message ?>
        </div>
    <?php endif; ?>

    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors->all() as $error): ?>
                    <li><?php echo $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <h1>Contact Form</h1>

    <form method="post">
        <?php echo csrf_field() ?>

        @include('forms.text', [
            'label' => 'Name',
            'name' => 'name'
        ])

        @include('forms.text', [
            'label' => 'Email',
            'name' => 'email'
        ])

        @include('forms.text', [
            'label' => 'Bogus',
            'name' => 'bogus'
        ])

        <textarea name="message" rows="8" cols="80" placeholder="message"><?php echo old('message') ?></textarea>

        <input type="submit" name="" value="Submit">
    </form>

    <h2>Contacts</h2>
    <ul>
        <?php foreach ($contacts as $contact): ?>
            <li>{{ $contact->name }}</li>
        <?php endforeach; ?>
    </ul>
@endsection
