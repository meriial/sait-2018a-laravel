@extends('layout')

@section('other')
    <h2>hahaha</h2>
@endsection

@section('content')
    <header class="bg1">
        <div class="">
            <div class="container">
                div 1
            </div>
        </div>
        <div class="header-image">
            <img src="https://pbs.twimg.com/profile_banners/25073877/1520088304/1500x500" alt="">
        </div>
        <div class="padding-bottom-5 padding-top-5">
            <div class="container flex-horizontal flex">
                <?php foreach ($metrics as $metric): ?>
                    @include('user_metrics')
                <?php endforeach; ?>
            </div>
        </div>
    </header>

    <h1>Twitter</h1>
    <a href="/tweet" class="btn btn-primary">Create Tweet</a>
    <h2>Users</h2>
    <ul>
        <?php foreach($users as $user): ?>
            <li>
                <?php echo $user->name ?>
                <?php echo $user->handle ?>
                This user likes <?php echo count($user->likes) ?> tweets.
                <ul>
                    <?php foreach ($user->tweets as $tweet): ?>
                        <li><?php echo $tweet->content ?></li>
                        <li>
                            This tweet liked by <?php echo count($tweet->likes) ?> users.
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php endforeach; ?>
    </ul>

    <h2>Tweets</h2>
    <ul>
        <?php foreach($tweets as $tweet): ?>
            <li>
                @include('partials.tweet')
            </li>
        <?php endforeach; ?>
    </ul>
@endsection
