@extends('layout')

@section('content')
    <form method="post">
        <?php echo csrf_field() ?>

        @include('forms.text', [
            'label' => 'Content',
            'name' => 'content'
        ]);

        <input type="submit" name="" value="Submit">
    </form>
@endsection
